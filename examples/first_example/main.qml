import QtQuick 2.15
import QtQuick.Window 2.15
import QtGraphicalEffects 1.15
//import QtQuick.Shapes 1.1
//import QtQuick.Controls 1.4
import QtQuick.Controls 2.15
//import QtQuick.Layouts 1.3
//import QtQuick.Dialogs 1.2
//import QtQml.Models 2.15
//import QtGraphicalEffects 1.12

import QtMultimedia 5.15

/*import widgets.sensorsListWidget 1.0 as SenSorsList
import widgets.cicularGaugePower 1.0 as CircularGaugePower
import widgets.cicularGauge 1.0 as CircularGauge
import widgets.indicatorSwitchWidget 1.0 as  IndicatorSwitchWidget
import widgets.logWidget 1.0 as LogWidget*/

import widgets.cicularGauge 1.0 as CircularGauge
Window {
    id: window
    width: 400
    height: 700
    visible: true

    Audio {
           id: myAudio
           source: "bubbleSound.mp3"

//           function playSound() {
//               myAudio.play()
//           }
       }


    property color bgcolor: "red"
    Item{
         anchors.fill: parent
        LinearGradient {
               anchors.fill: parent
               start: Qt.point(200, 0)
               end: Qt.point(200, 700)
               gradient: Gradient {
                   GradientStop { position: 0.0; color: window.bgcolor }
                   GradientStop { position: 1.3; color: "black" }
               }
           }


        CircularGauge.CicularGauge {
            id: progress
            height: 400
            width: window.width

            property int valval
            property int savevalval
            property bool isPomodoro : false
            property bool timerisRunning : false


            property int minValueOfcicularGauge: 0
            property int maxValueOfcicularGauge: 200

        }
    }

    Item {
        id:startButton

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        visible: true

        Rectangle{
            x: - 50

            width: 100
            height: 100
            radius:300
            color: "white"
            Text{
                id: startText
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 16
                text: "Старт"
            }

            MouseArea{
                anchors.fill : parent
                onClicked: {

                    progress.isPomodoro = true
                    startButton.visible = false
                    stopButton.visible = true

                    progress.timerisRunning = true
                }
            }
        }


    }

    Item {
        id:stopButton

        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter

        visible: false

        Rectangle{
            x: - 50

            width: 100
            height: 100
            radius:300
            color: "white"
            Text{
                id: stopText
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pointSize: 16
                text: "Стоп"
            }

            MouseArea{
                anchors.fill : parent
                onClicked: {
                    progress.isPomodoro = false
                    startButton.visible = true
                    stopButton.visible = false
                }
            }
        }
    }


    Grid {
        columns: 4
        spacing: 2

        anchors.bottom: parent.bottom

        Button {
            text: "5"
            onClicked: {

                progress.valval = 300
                progress.savevalval = 300

                progress.maxValueOfcicularGauge = 300
                window.bgcolor = "red"
                progress.isPomodoro = false

                startButton.visible = true
                stopButton.visible = false

                progress.timerisRunning = true

            }
        }
        Button {
            text: "10"
            onClicked: {

                progress.valval = 600
                progress.savevalval = 600

                progress.maxValueOfcicularGauge = 600
                window.bgcolor = "red"
                progress.isPomodoro = false

                startButton.visible = true
                stopButton.visible = false

                progress.timerisRunning = true



            }
        }
        Button {
            text: "15"
            onClicked: {

                progress.valval = 900
                progress.savevalval = 900

                progress.maxValueOfcicularGauge = 900
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

                progress.timerisRunning = true



            }
        }
        Button {
            text: "20"
            onClicked: {

                progress.valval = 1200
                progress.savevalval = 1200

                progress.maxValueOfcicularGauge = 1200
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }

        Button {
            text: "25"
            onClicked: {

                progress.valval = 1500
                progress.savevalval = 1500

                progress.maxValueOfcicularGauge = 1500
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "30"
            onClicked: {

                progress.valval = 1800
                progress.savevalval = 1800

                progress.maxValueOfcicularGauge = 1800
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "35"
            onClicked: {

                progress.valval = 2100
                progress.savevalval = 2100

                progress.maxValueOfcicularGauge = 2100
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "40"
            onClicked: {

                progress.valval = 2400
                progress.savevalval = 2400

                progress.maxValueOfcicularGauge = 2400
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "45"
            onClicked: {

                progress.valval = 2700
                progress.savevalval = 2700

                progress.maxValueOfcicularGauge = 2700
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "50"
            onClicked: {

                progress.valval = 3000
                progress.savevalval = 3000

                progress.maxValueOfcicularGauge = 3000
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "55"
            onClicked: {

                progress.valval = 3300
                progress.savevalval = 3300

                progress.maxValueOfcicularGauge = 3300
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }
        Button {
            text: "60"
            onClicked: {

                progress.valval = 3600
                progress.savevalval = 3600

                progress.maxValueOfcicularGauge = 3600
                window.bgcolor = "red"
                progress.isPomodoro = false
                startButton.visible = true
                stopButton.visible = false

            }
        }



    }
}

