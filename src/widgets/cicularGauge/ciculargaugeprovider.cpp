#include "ciculargaugeprovider.h"

#include <QtQml/QtQml>
#include <QPen>
#include <QPainter>
#include <QtDebug>
#include "iostream"



PomodoroController::PomodoroController(QObject *parent) : QObject(parent) {

    timer = new QTimer();
    timer->start(1000);
    QObject::connect(timer, &QTimer::timeout, [this]() { this->timerExpired();});
}

void PomodoroController::setPomodoroTime(const int& pomodoroTimer)
{
    _pomodoroTimer = pomodoroTimer;
    std::cout<< _pomodoroTimer << std::endl;
}

void PomodoroController::setPomodoroOn(const bool &isPomodoroOn)
{
    _isPomodoroOn = isPomodoroOn;
}

int PomodoroController::getPomodoroTime()
{
    return _pomodoroTimer;
}
bool PomodoroController::getPomodoroOn()
{
    return _isPomodoroOn;
}

void PomodoroController::timerExpired() {
    if(_pomodoroTimer > 0 && _isPomodoroOn){
        _pomodoroTimer--;
    }
    if(_pomodoroTimer == 0){

    }
}



