#ifndef MYQUICKITEM_H
#define MYQUICKITEM_H

#include <QtQuick>

#include <QObject>
#include <QTimer>
#include <QVector>
#include <QMap>
//#include <UInterface.h>
//#include <UniSetTypes.h>
#include <QString>

/**
 * @brief The MyQuickItem class. Simple QQuickItem plugin example;
 */
class PomodoroController:  public QObject {
    Q_OBJECT
    Q_PROPERTY(int pomodoroTime WRITE setPomodoroTime READ getPomodoroTime)
    Q_PROPERTY(int isPomodoroOn  WRITE setPomodoroOn READ getPomodoroOn)

public:
   explicit PomodoroController(QObject *parent = nullptr);



  int  getPomodoroTime();
  bool getPomodoroOn();

  void setPomodoroTime(const int& pomodoroTimer);
  void setPomodoroOn(const bool& isPomodoroOn);
  void timerExpired();

Q_SIGNALS:

private:
    QTimer *timer;
    QString _sensorName;
    int _pomodoroTimer ;
    int pomodoroSeconds;
    bool _isPomodoroOn = false;
};

#endif // MYQUICKITEM_H
