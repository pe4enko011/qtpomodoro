import QtQuick 2.15
import QtQuick.Window 2.15
import QtQuick.Shapes 1.15
import QtGraphicalEffects 1.15
import QtGraphicalEffects 1.12
import QtQuick.Controls 2.15

//import QtGraphicalEffects 1.15
import widgets.cicularGauge 1.0 as CircularGauge

Item {
    id: progress

    CircularGauge.CicularGaugeProvider {
        id: cicularGaugeProvider
        pomodoroTime: progress.valval
        isPomodoroOn : progress.isPomodoro


    }

    implicitWidth: parent.width //400
    implicitHeight: parent.height //400


    //property int valfromQt : cicularGaugeProvider.pomodoroTime


    property int sweepAngleOfsetPointer: 100

    // Properties
    property int limitValue

    //property int minValueOfcicularGauge
    //property int maxValueOfcicularGauge
    property double limitAngle: progress.limitValue * 279 / progress.maxValueOfcicularGauge
    // General
    property bool roundCap: false
    property int startAngle: -230
    property real maxValue: 118
    property real value: 0
    property int samples: 12

    // Drop Shadowr
    property bool enableDropShadow: true
    property color dropShadowColor: "#20000000"
    property int dropShadowRadius: 10

    // Bg Circle
    property color bgColor: "transparent"
    property color bgStrokeColor: "black"
    property int strokeBgWidth: 25

    // Progress Circle
    property string progressColor: Qt.rgba(0, 255, 0,  0.1)
    property int progressWidth: 25

    // Text
    property string minus: ""
    property string text: ""
    property bool textShowValue: true
    property string textFontFamily: "Segoe UI"
    property int textSize: 75
    property color textColor: "black"
    // Angle
    property int sweepAngle: 0

    Timer {
        id: timer
        interval: 40
        running: progress.timerisRunning
        repeat: true
        onTriggered: {

            progress.sweepAngle = 279 - cicularGaugeProvider.pomodoroTime * 279 / progress.maxValueOfcicularGauge
            textProgress.text = Math.floor(cicularGaugeProvider.pomodoroTime / 60) + ":" +
                    (cicularGaugeProvider.pomodoroTime % 60 >= 10 ?  Math.floor(cicularGaugeProvider.pomodoroTime % 60) : "0" + Math.floor(cicularGaugeProvider.pomodoroTime % 60))

            if(cicularGaugeProvider.pomodoroTime === 0){

                progress.valval = 12
                progress.valval = 1
                progress.isPomodoro = false


                startButton.visible = true
                stopButton.visible = false
                progress.sweepAngle = 0

                window.bgcolor = "green"
                 myAudio.play()

                progress.timerisRunning = false

                console.log("dsd")


            }

        }
    }

    // progress.sweepAngle: cicularGaugeProvider.

    // Internal Properties/Functions
    QtObject {
        id: internal



        //        property Component dropShadow: DropShadow{
        //            color: progress.dropShadowColor
        //            fast: true
        //            verticalOffset: 0
        //            horizontalOffset: 0
        //            samples: progress.samples
        //            radius: progress.dropShadowRadius
        //        }
    }

    Shape {
        id: shape
        anchors.fill: parent
        layer.enabled: true
        layer.samples: progress.samples

        //layer.effect: progress.enableDropShadow ? internal.dropShadow : null
        ShapePath {
            id: pathBG
            strokeColor: progress.bgStrokeColor
            fillGradient : progress.bgColor
            fillColor: progress.bgColor
            strokeWidth: progress.strokeBgWidth
            capStyle: progress.roundCap ? ShapePath.RoundCap : ShapePath.FlatCap


            PathAngleArc {
                radiusX: (progress.width / 2) - (progress.progressWidth / 2) - 25
                radiusY: (progress.height / 2) - (progress.progressWidth / 2) - 25
                centerX: progress.width / 2
                centerY: progress.height / 2
                startAngle: progress.startAngle
                sweepAngle: 279
            }
        }

        ShapePath {
            id: setVaiblePover
            strokeColor: "#008500"
            fillColor: "#008500"
            strokeWidth: 0

            capStyle: progress.roundCap ? ShapePath.RoundCap : ShapePath.FlatCap

            PathAngleArc {
                radiusX: (progress.width / 2) - (progress.progressWidth / 2) - 25
                radiusY: (progress.height / 2) - (progress.progressWidth / 2) - 25
                centerX: progress.width / 2
                centerY: progress.height / 2
                startAngle: progress.startAngle
                sweepAngle: (progress.limitAngle)
            }
        }



        // Текущее значение

        ShapePath {
            id: setPointer
            strokeColor: "white"
            fillColor: "transparent"

            strokeWidth: progress.progressWidth
            capStyle: progress.roundCap ? ShapePath.RoundCap : ShapePath.FlatCap

            PathAngleArc {
                radiusX: (progress.width / 2) - (progress.progressWidth / 2) - 25
                radiusY: (progress.height / 2) - (progress.progressWidth / 2) - 25
                centerX: progress.width / 2
                centerY: progress.height / 2
                startAngle: progress.startAngle
                sweepAngle: (progress.sweepAngle)
            }
        }

        Text {
            id: textProgress
            text: "25:00"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            color: progress.textColor
            font.pointSize: progress.width > 0 ? progress.width / 5.5 : 100
            font.family: progress.textFontFamily
        }
        //        Text {
        //            id: transcriptOftextProgress
        //            text: "кВт"
        //            color: progress.textColor
        //            font.pointSize: progress.width > 0 ? progress.width / 20 : 100
        //            anchors.bottom: parent.bottom
        //            anchors.bottomMargin: parent.width / 3.7
        //            //anchors.verticalCenter: parent.verticalCenter
        //            //anchors.horizontalCenter: parent.horizontalCenter
        //        }

    }

}
