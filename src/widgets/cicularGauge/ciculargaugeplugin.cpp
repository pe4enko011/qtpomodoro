#include "ciculargaugeplugin.h"

#include <QtQml/QtQml>

#include "ciculargaugeprovider.h"

void MyPlugin::registerTypes(const char *uri) {
  qmlRegisterType<PomodoroController>(uri, 1, 0, "CicularGaugeProvider");
}
